import { prisma } from "../../../../database/prismaClient"
import { hash } from 'bcrypt'

interface ICreateClient {
  username: string
  password: string
}

export class CreateClienteUseCase {
  async execute({ username, password }:ICreateClient) {
    const clientExist = await prisma.clients.findFirst({
      where: {
        username: {
          equals: username,
          mode: "insensitive"
        }
      }
    })

    if (clientExist) {
      throw new Error('Client already exists')
    }

    const hasPassword = await hash(password, 12)

    const client = await prisma.clients.create({
      data: {
        username,
        password: hasPassword
      }
    })

    return client
  }
}