import { Router } from "express"

import { CreateClientController } from "./models/clients/useCases/createClient/CreateClientController"
import { AuthenticateClientController } from './models/account/authenticateClient/AuthenticateClientController';
import { ensureAuthenticateClient } from './middlewares/ensureAuthenticateClient';
import { FindAllDeliveriesController } from "./models/clients/useCases/deliveries/FindAllDeliveriesController";

import { CreateDeliverymanController } from './models/deliveryman/useCases/createDeliveryman/CreateDeliverymanController';
import { AuthenticateDeliverymanController } from './models/account/authenticateDeliveryman/AuthenticateDeliverymanController';
import { CreateDeliveryController } from './models/deliveries/useCases/createDelivery/CreateDeliveryController';
import { FindAllDeliveriesDeliverymanController } from "./models/deliveryman/useCases/findAllDelivries/FindAllDelivriesDeliverymanController";

import { FindAllAvailableController } from './models/deliveries/useCases/findAllAvailable/FindAllAvailableController';
import { ensureAuthenticateDeliveryman } from "./middlewares/ensureAuthenticateDeliveryman";
import { UpdateDeliverymanController } from "./models/deliveries/useCases/updateDeliveryman/UpdateDeliverymanController";
import { UpdateEndDateController } from "./models/deliveries/useCases/updateEndDate/UpdateEndDateController";

const routes = Router()

const createClientController = new CreateClientController()
const authenticateClientController = new AuthenticateClientController()
const findAllDeliveriesClient = new FindAllDeliveriesController()

const createDeliverymanController = new CreateDeliverymanController()
const authenticateDeliverymanController = new AuthenticateDeliverymanController()
const updateDeliverymanController = new UpdateDeliverymanController()
const findAllDeliveriesDeliveryman = new FindAllDeliveriesDeliverymanController()

const deliveryController = new CreateDeliveryController()
const finAllAvailableController = new FindAllAvailableController()
const updateEndDateController = new UpdateEndDateController()

routes.post('/client/', createClientController.handle)

routes.post('/client/login/', authenticateClientController.handle)

routes.get('/client/deliveries', ensureAuthenticateClient, findAllDeliveriesClient.handle)

routes.post('/deliveryman/', createDeliverymanController.handle)

routes.get('/deliveryman/deliveries', ensureAuthenticateDeliveryman, findAllDeliveriesDeliveryman.handle)

routes.post('/deliveryman/login/', authenticateDeliverymanController.handle)

routes.post('/delivery', ensureAuthenticateClient, deliveryController.handle)

routes.get('/delivery/available', ensureAuthenticateDeliveryman, finAllAvailableController.handle)

routes.put('/delivery/update/:id', ensureAuthenticateDeliveryman, updateDeliverymanController.handle)

routes.put('/delivery/endDate/:id', ensureAuthenticateDeliveryman, updateEndDateController.handle)

export { routes }